#!/bin/bash

# Version 0.0.1
# Backup script to export the project database and the phpcr structure.

BASEPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"/

newsDirectory=$BASEPATH'news'
spidersDirectory=$BASEPATH'hockeyservice/spiders/'

newsSpiderHockeyInfo=$spidersDirectory'/NewsSpiderHockeyinfo.py'
newsSpiderBlick=$spidersDirectory'/NewsSpiderBlick.py'

scrapyCommand=$BASEPATH'env/bin/scrapy'

mkdir -p $newsDirectory
rm -f $newsDirectory'/news-hockey-info-temp.json'
rm -f $newsDirectory'/news-blick-temp.json'

$scrapyCommand runspider $newsSpiderHockeyInfo -o $newsDirectory'/news-hockey-info-temp.json'
file=$newsDirectory'/news-hockey-info-temp.json'
if [ -s "$file" ]
then
    rm -f $newsDirectory'/news-hockey-info.json'
    mv $newsDirectory'/news-hockey-info-temp.json' $newsDirectory'/news-hockey-info.json'
else
   echo " file does not exist, or is empty "
fi

$scrapyCommand runspider $newsSpiderBlick -o $newsDirectory'/news-blick-temp.json'
file=$newsDirectory'/news-blick-temp.json'
if [ -s "$file" ]
then
    rm -f $newsDirectory'/news-blick.json'
    mv $newsDirectory'/news-blick-temp.json' $newsDirectory'/news-blick.json'
else
   echo " file does not exist, or is empty "
fi

from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.standings import Standings

class StandingsView(BaseView):

    REDIS_KEY = 'standings-'
    REDIS_CACHE_TIME = 18000000 #5hrs

    def __init__(self, request):
        super(StandingsView, self).__init__()
        self.request = request
        self.standings = Standings()

    @view_config(route_name='standings', renderer='jsonp')
    def fetchStandings(self):
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.standings.retrieveStandingsByLeague(league)
        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

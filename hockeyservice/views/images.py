from pyramid.view import view_config
from pyramid.response import FileResponse
from pyramid.response import Response
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.images import HockeyDataImages

class HockeyDataImagesView(BaseView):

    # Init the class.
    def __init__(self, request):
        super(HockeyDataImagesView, self).__init__()
        self.request = request
        self.imageService = HockeyDataImages()

    # Fetches from game api with given gameId.
    #
    # @param string gameId
    #
    # @return dict
    @view_config(route_name='teamLogo')
    def fetchTeamLogo(self):
        cacheLifetime = 31536000
        imageName = self.request.matchdict['imageName']
        image = self.imageService.retrieveTeamImageByName(imageName)

        return FileResponse(image,
                            request=self.request,
                            cache_max_age=cacheLifetime,
                            content_type='image/png'
                            )

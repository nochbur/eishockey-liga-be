from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView

class LeaguesView(BaseView):

    # Init the class.
    def __init__(self, request):
        super(LeaguesView, self).__init__()
        self.request = request

    # Returns the league config.
    #
    # @return dict
    @view_config(route_name='leagues', renderer='jsonp')
    def fetchLeagues(self):
        config = self.hockeyDataService.getLeagueConfig()

        return self.renderJsonResponse(config)

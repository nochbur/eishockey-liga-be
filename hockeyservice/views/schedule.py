from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.schedule import Schedule

class ScheduleView(BaseView):

    REDIS_KEY = 'standings-'
    REDIS_CACHE_TIME = 180000 #3min

    # Init the class.
    def __init__(self, request):
        super(ScheduleView, self).__init__()
        self.request = request
        self.schedule = Schedule()

    # Returns a list of all schedule items grouped by their gameday date.
    #
    # @param string id
    #
    # @return dict
    @view_config(route_name='scheduleList', renderer='jsonp')
    def fetchScheduleList(self):
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + 'all-' + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.schedule.retrieveScheduleByLeague(league)
        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

    # Fetches from the schedule api and returns a list of all pending schedule items
    # a given leagues. Grouped by their gameday date.
    #
    # @param string id
    #
    # @return dict
    @view_config(route_name='scheduleNextList', renderer='jsonp')
    def fetchScheduleNextList(self):
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + 'next-' + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.schedule.retrieveScheduleNextByLeague(league)
        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

    # Fetches from the schedule api and returns a list of all latest schedule items
    # a given leagues. Grouped by their gameday date.
    #
    # @param string id
    #
    # @return dict
    @view_config(route_name='scheduleLatestList', renderer='jsonp')
    def fetchScheduleLatestList(self):
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + 'latest-' + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.schedule.retrieveScheduleLatestByLeague(league)
        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

    # Fetches from the schedule api and returns a list of all todays schedule items
    # Grouped by their gameday date.
    #
    # @param string id
    #
    # @return dict
    @view_config(route_name='scheduleTodayList', renderer='jsonp')
    def fetchScheduleToday(self):
        cacheLifeTime = 60000 #1min
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + 'today-' + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.schedule.retrieveScheduleTodayByLeague(league)
        self.setCachedRedisDataByKey(key, result, cacheLifeTime)

        return self.renderJsonResponse(result)

    # Fetches all schedule lists for all leagues to do a cache warmup.
    #
    # @return dict
    @view_config(route_name='scheduleCacheWarmup', renderer='jsonp')
    def doScheduleCacheWarmup(self):
        leagues = self.hockeyDataService.getLeagueConfig()

        for league in leagues:
            self.schedule.retrieveScheduleListFlatByLeague(league, True)

        return self.renderJsonResponse('Done')

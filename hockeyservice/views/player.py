from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.player import Player

class PlayerView(BaseView):

    REDIS_KEY = 'player-'
    REDIS_CACHE_TIME = 18000000 #5hrs

    def __init__(self, request):
        super(PlayerView, self).__init__()
        self.request = request
        self.player = Player()

    @view_config(route_name='player', renderer='jsonp')
    def fetchPlayerData(self):
        leagueId = self.request.matchdict['id']
        playerId = self.request.matchdict['playerId']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league or not playerId:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + str(leagueId) + '-' + str(playerId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.player.retrievePlayerById(leagueId, playerId)

        if not result:
            return self.renderJsonResponse(result)

        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

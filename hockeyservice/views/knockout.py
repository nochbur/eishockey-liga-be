from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.knockout import Knockout

class KnockoutView(BaseView):

    REDIS_KEY = 'knockout-'
    REDIS_CACHE_TIME = 18000000 #5hrs

    # Init the class.
    def __init__(self, request):
        super(KnockoutView, self).__init__()
        self.request = request
        self.knockout = Knockout()

    # Fetches from knockout api with given id.
    #
    # @param string id
    #
    # @return dict
    @view_config(route_name='knockout', renderer='jsonp')
    def fetchKnockout(self):
        leagueId = self.request.matchdict['id']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + str(leagueId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.knockout.retrieveKnockoutByLeague(league)

        if not result:
            return self.renderJsonResponse(result)

        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

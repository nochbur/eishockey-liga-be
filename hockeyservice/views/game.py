from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.game import Game

class GameView(BaseView):

    REDIS_KEY = 'schedule-'
    REDIS_CACHE_TIME = 60000 #1min

    # Init the class.
    def __init__(self, request):
        super(GameView, self).__init__()
        self.request = request
        self.game = Game()

    # Fetches from game api with given gameId.
    #
    # @param string gameId
    #
    # @return dict
    @view_config(route_name='gameDetailView', renderer='jsonp')
    def fetchGame(self):
        gameId = self.request.matchdict['gameId']

        if not gameId:
            errorMsg = 'Missing gameId.'
            return self.renderJsonErrorResponse(errorMsg)

        key = self.REDIS_KEY + 'item-' + str(gameId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.game.retrieveGameById(gameId)

        if not result:
            return self.renderJsonResponse(result)

        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView
from hockeyservice.hockeydata.team import Team

class TeamView(BaseView):

    REDIS_KEY = 'team-'
    REDIS_CACHE_TIME = 18000000 #5hrs

    def __init__(self, request):
        super(TeamView, self).__init__()
        self.request = request
        self.team = Team()

    @view_config(route_name='team', renderer='jsonp')
    def fetchTeamData(self):
        leagueId = self.request.matchdict['id']
        teamId = self.request.matchdict['teamId']
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league or not teamId:
            return self.renderLeagueNotFoundError(leagueId)

        key = self.REDIS_KEY + str(leagueId) + '-' +  str(teamId)
        redisData = self.getCachedRedisDataByKey(key)
        if redisData:
            return self.renderJsonResponse(redisData)

        result = self.team.retrieveTeamById(leagueId, teamId)
        self.setCachedRedisDataByKey(key, result, self.REDIS_CACHE_TIME)

        return self.renderJsonResponse(result)

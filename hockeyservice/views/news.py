import os
import json
from pyramid.view import view_config
from hockeyservice.services.baseview import BaseView

class NewsView(BaseView):

    # Init the class.
    def __init__(self, request):
        super(NewsView, self).__init__()
        self.request = request

    # Returns all news
    #
    # @return dict
    @view_config(route_name='fetchNews', renderer='jsonp')
    def fetchNews(self):
        result = {}
        news = {}

        news['AT'] = self.getNewsJsonForFile('news-hockey-info.json')
        news['CH'] = self.getNewsJsonForFile('news-blick.json')

        result['news'] = news

        return self.renderJsonResponse(result)

    # @param string fileName
    #
    # @return json
    def getNewsJsonForFile(self, fileName):
        try:
            here = os.path.abspath(os.path.dirname(__file__))
            fileName = '../../news/' + fileName

            with open(os.path.join(here, fileName)) as data_file:
                data = json.load(data_file)

            return data
        except FileNotFoundError:
            return []

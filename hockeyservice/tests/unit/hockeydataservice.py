import urllib
from unittest import TestCase
from hockeyservice.services.hockeydataservice import HockeyDataService

class HockeyDataServiceTest(TestCase):
    """Tests for the hockeydata service"""

    def setUp(self):
        self.service = HockeyDataService()

    def test_retrieveUrlParams(self):
        # If no params are given the default url params are returned.
        params = None
        expectedOutput = urllib.parse.urlencode(self.service.defaultUrlParams)
        self.assertEqual(self.service.retrieveUrlParams(params), expectedOutput)

        # It is not possible to provide something else than a dict as params.
        # The default set of params will be returned.
        params = 'string'
        expectedOutput = urllib.parse.urlencode(self.service.defaultUrlParams)
        self.assertEqual(self.service.retrieveUrlParams(params), expectedOutput)

        params = 1
        expectedOutput = urllib.parse.urlencode(self.service.defaultUrlParams)
        self.assertEqual(self.service.retrieveUrlParams(params), expectedOutput)

        params = [1, 2, 3]
        expectedOutput = urllib.parse.urlencode(self.service.defaultUrlParams)
        self.assertEqual(self.service.retrieveUrlParams(params), expectedOutput)

        # If a valid dict is provided the key is added to the returned string.
        params = {
            'test': 'me'
        }
        output = self.service.retrieveUrlParams(params)
        output = urllib.parse.parse_qs(output)
        self.assertTrue('test' in output.keys())

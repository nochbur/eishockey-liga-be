import fakeredis
from unittest import TestCase
from hockeyservice.services.storage import Storage

class StorageTest(TestCase):
    """Tests for the storage service"""

    def setUp(self):
        # Setup fake redis for testing.
        self.storage = Storage()
        self.storage.storage = fakeredis.FakeStrictRedis()

    def tearDown(self):
        self.storage.storage.flushall()

    def test_getJsonStorageByKey(self):
        # If no key is provided None is returned.
        key = None
        expectedOutput = key
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # If the key is not found none is returned.
        key = 'test-invalid'
        expectedOutput = None
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # If the key exists a valid json is returned.
        key = 'test-valid'
        expectedOutput = {
            'test': 'me'
        }
        self.storage.setJsonStorageByKey(key, expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # If the store is flushed the key does no longer exist.
        # If the key exists a valid json is returned.
        self.storage.storage.flushall()
        expectedOutput = None
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

    def test_setJsonStorageByKey(self):
        # If no key is given, none is returned.
        key = None
        value = {
            'test': 'me'
        }
        expectedOutput = None
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # If no value is given, none is returned.
        key = 'test-invalid'
        value = None
        expectedOutput = None
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # It is possible to store strings.
        key = 'test-valid'
        value = 'test'
        expectedOutput = value
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # Strings are utf8 encoded.
        key = 'test-valid'
        value = 'testä'
        expectedOutput = value
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # It is possible to store numbers.
        key = 'test-valid'
        value = 1
        expectedOutput = value
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

        # It is possible to store objects.
        key = 'test-valid'
        value = {
            'test': 'me'
        }
        expectedOutput = value
        self.assertEqual(self.storage.setJsonStorageByKey(key, value), expectedOutput)
        self.assertEqual(self.storage.getJsonStorageByKey(key), expectedOutput)

    def test_getCachedRedisDataByKey(self):
        # If no key is given, none is returned.
        key = None
        expectedOutput = None
        self.assertEqual(self.storage.getCachedRedisDataByKey(key), expectedOutput)

        # If the key does not exist, None is returend.
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.storage.getCachedRedisDataByKey(key), expectedOutput)

        # If the key exists and the max_cache_time is expired, None is returned.
        key = 'valid'
        value = 'test'
        max_cache_time = -1
        expectedOutput = None
        self.storage.setCachedRedisDataByKey(key, value, max_cache_time)
        self.assertEqual(self.storage.getCachedRedisDataByKey(key), expectedOutput)

        # If the key exists and is not expired, the value is returned
        key = 'valid'
        value = 'test'
        expectedOutput = value
        self.storage.setCachedRedisDataByKey(key, value)
        self.assertEqual(self.storage.getCachedRedisDataByKey(key), expectedOutput)


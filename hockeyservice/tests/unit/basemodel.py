from unittest import TestCase
from hockeyservice.model.basemodel import BaseModel

class BaseModelTest(TestCase):
    """Tests for the basemodel"""

    def setUp(self):
        self.basemodel = BaseModel()

    def test_getProperty(self):
        # If called without any params None is returned.
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(), expectedOutput)

        # If called with None values, None is returned.
        item = None
        key = None
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        # If called with no item but some property None is returned.
        item = None
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        # If called with something other than a list or object as item, None is returned.
        item = 'invalid'
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        item = 1
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        # If called with an empty list, None is returned.
        item = []
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        item = {}
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        # If key is not found, None is returned.
        item = [1, 2, 3]
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        item = { 'test': 'me' }
        key = 'invalid'
        expectedOutput = None
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        # Optional some fallback value can be provided.
        item = [1, 2, 3]
        key = 'invalid'
        expectedOutput = 'invalid'
        self.assertEqual(self.basemodel.getProperty(item, key, expectedOutput), expectedOutput)

        # Existing properties are returned correctly.
        item = [1, 2, 3]
        key = 1
        expectedOutput = 2
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

        item = { 'test': 'me' }
        key = 'test'
        expectedOutput = 'me'
        self.assertEqual(self.basemodel.getProperty(item, key), expectedOutput)

from unittest import TestCase
from hockeyservice.services.restservice import RestService

class RestServiceTest(TestCase):
    """Tests for the rest service"""

    restservice = RestService()
    expectedOutput = {
        'status': 200,
        'data': None
    }

    def test_renderJsonResponse(self):
        self.expectedOutput['status'] = 200

        # If no params are provided None is returned as data parameter.
        # Status is expected to be 200.
        data = None
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonResponse(), self.expectedOutput)

        # If no data are provided None is returned as data parameter.
        # Status is expected to be 200.
        data = None
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonResponse(data), self.expectedOutput)

        # If a string is provided the string is added to the data parameter.
        # Status is expected to be 200.
        data = 'test'
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonResponse(data), self.expectedOutput)

        # Given objects are added to the data attribute of the response.
        # Status is expected to be 200.
        data = {
            'test': 'me'
        }
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonResponse(data), self.expectedOutput)

    def test_renderJsonErrorResponse(self):
        self.expectedOutput['status'] = 400

        # If called without any parameter a default error text is added.
        # Status is expected to be 400.
        data = self.restservice.DEFAULT_API_ERROR
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonErrorResponse(), self.expectedOutput)

        # If called with an empty string a default error text is added.
        # Status is expected to be 400.
        data = ''
        self.expectedOutput['data'] = self.restservice.DEFAULT_API_ERROR
        self.assertEqual(self.restservice.renderJsonErrorResponse(data), self.expectedOutput)

        # If a custom text is given it is also added to the data parameter.
        # Status is expected to be 400.
        data = 'Custom Error happend'
        self.expectedOutput['data'] = data
        self.assertEqual(self.restservice.renderJsonErrorResponse(data), self.expectedOutput)

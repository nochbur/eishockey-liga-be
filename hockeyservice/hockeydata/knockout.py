from hockeyservice.services.baseview import BaseView
from hockeyservice.model.scheduleitem import ScheduleItemModel

class Knockout(BaseView):

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    #
    # @return dict
    def retrieveKnockoutByLeague(self, league = None):
        if not league:
            return None

        result = {
            'league': league['title'],
            'playoff': None
        }

        if self.getProperty(league, 'playoff'):
            divisionId = league['playoff']['divisionId']
            data = self.hockeyDataService.fetchHockeyDataKnockout(divisionId)

            if not data:
                return None

            result['playoff'] = self.retrieveReducedData(data)

        return result

    # Returns a reduced api structure.
    #
    # @param list data
    #
    # @return list
    def retrieveReducedData(self, data = []):
        result = []

        for phase in data:
            phaseItem = self.getDefaultPhaseStructure()
            phaseItem['divisionId'] = self.getProperty(phase, 'divisionId')
            phaseItem['divisionName'] = self.getProperty(phase, 'divisionName')

            for encounter in self.getProperty(phase, 'encounters', []):
                encounterItem = self.getDefaultEncounterStructure()
                encounterItem['id'] = self.getProperty(encounter, 'id')
                encounterItem['longname'] = self.getProperty(encounter, 'longname')
                encounterItem['bestOf'] = self.getProperty(encounter, 'bestOf')
                encounterItem['teams'] = self.getProperty(encounter, 'teams')
                phaseItem['hasStarted'] = self.hasEncounterStarted(encounter)

                for game in self.getProperty(encounter, 'games', []):
                    gameItem = ScheduleItemModel(game)
                    encounterItem['games'].append(gameItem.toDict())

                phaseItem['encounters'].append(encounterItem)

            result.append(phaseItem)

        return result

    # Returns if the encounter has started.
    #
    # @return boolean
    def hasEncounterStarted(self, encounter = {}):
        defaultTeamNames = [
            'FIN Team 1',
            'FIN Team 2',
            'HF1 Team 1',
            'HF1 Team 2',
            'HF2 Team 1',
            'HF2 Team 2',
            'VF1 Team 1',
            'VF1 Team 2',
            'VF2 Team 1',
            'VF2 Team 2',
            'VF3 Team 1',
            'VF3 Team 2',
            'VF4 Team 1',
            'VF4 Team 2',
            'SF1 Team 1',
            'SF1 Team 2',
            'SF2 Team 1',
            'SF2 Team 2',
            'Final Team 1',
            'Final Team 2',
        ]
        result = True

        for team in self.getProperty(encounter, 'teams', []):
            if team['longname'] in defaultTeamNames:
                result = False

        return result

    # Returns a default phase structure.
    #
    # @return dict
    def getDefaultPhaseStructure(self):
        return {
            'divisionId': None,
            'divisionName': None,
            'encounters': []
        }

    # Returns a default encounter structure.
    #
    # @return dict
    def getDefaultEncounterStructure(self):
        return {
            'id': None,
            'longname': None,
            'bestOf': None,
            'teams': [],
            'games': []
        }

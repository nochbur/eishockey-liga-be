from hockeyservice.services.baseview import BaseView

class Player(BaseView):

    playerData = None
    playerStats = None
    games = None
    divisionId = None
    images = None

    # Fetches data from api and prepares them to be responded.
    #
    # @param string leagueId
    # @param string playerId
    #
    # @return dict
    def retrievePlayerById(self, leagueId = None, playerId = None):
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league or not playerId or not leagueId:
            return None

        result = {
            'player': None
        }
        divisionId = league['preliminary']['divisionId']
        data = self.hockeyDataService.fetchHockeyDataPlayer(divisionId, playerId)

        if not data:
            return None

        result['player'] = self.retrieveReducedData(data)

        return result

    # Returns a reduced api structure.
    #
    # @param list data
    #
    # @return list
    def retrieveReducedData(self, data = []):
        self.playerData = self.getProperty(data, 'playerData', {})
        self.playerStats = self.getProperty(data, 'playerStats', {})
        self.games = self.getProperty(data, 'games', [])
        self.divisionId = self.getProperty(data, 'divisionId', [])
        self.images = self.getProperty(data, 'images', [])

        result = {
            'playerData': self.retrieveReducedPlayerData(self.playerData),
            'playerStats': self.playerStats,
            'games': self.games,
            'divisionId': self.divisionId,
            'images': self.images,
        }

        return result

    # Returns a reduced api structure.
    #
    # @param dict data
    #
    # @return dict
    def retrieveReducedPlayerData(self, data = {}):
        player = {
            'id': self.getProperty(data, 'id'),
            'firstname': self.retrieveFormattedName(self.getProperty(data, 'playerFirstname')),
            'lastname': self.retrieveFormattedName(self.getProperty(data, 'playerLastname')),
            'number': self.getProperty(data, 'playerJerseyNr'),
            'position': self.getProperty(data, 'position'),
            'playerBirthdate': self.getProperty(data, 'playerBirthdate'),
            'nation': self.getProperty(data, 'nation'),
            'weight': self.getProperty(data, 'weight'),
            'height': self.getProperty(data, 'height'),
            'teamId': self.getProperty(data, 'teamId'),
            'teamLongname': self.getProperty(data, 'teamLongname'),
            'teamShortname': self.getProperty(data, 'teamShortname'),
            'teamFlavourname': self.getProperty(data, 'teamFlavourname'),
        }

        return player

    # Removes all unused characters from a given name string.
    #
    # @param string name
    #
    # @return string | None
    def retrieveFormattedName(self, name = ''):
        if not name:
            return None

        name = name.lower()
        name = name.replace('*', '')
        name = name.replace('**', '')
        name = name.replace('(f)', '')
        name = name.replace('(j)', '')
        name = name.replace('(koop)', '')
        return name.strip().title()

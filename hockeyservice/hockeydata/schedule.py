import json
import time
import datetime
from hockeyservice.services.baseview import BaseView
from hockeyservice.model.scheduleitem import ScheduleItemModel

class Schedule(BaseView):

    REDIS_KEY = 'schedule-list-flat-'
    REDIS_CACHE_TIME = 300000 #5min

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    #
    # @return dict
    def retrieveScheduleByLeague(self, league = None):
        if not league:
            return None

        result = {
            'league': self.getProperty(league, 'title'),
            'today': self.getTodaysTimestamp(),
            'preliminary': None,
            'intermediate': None,
            'playoff': None,
        }

        # Fetch all games in a flat list.
        games = self.retrieveScheduleListFlatByLeague(league)

        if games:
            preliminary = list(filter(lambda game: self.getProperty(game, 'leaguePhase') == 'preliminary', games))
            preliminary = self.retrieveGroupedGamedays(preliminary)
            result['preliminary'] = preliminary

            intermediate = list(filter(lambda game: self.getProperty(game, 'leaguePhase') == 'intermediate', games))
            intermediate = self.retrieveGroupedGamedays(intermediate)
            result['intermediate'] = intermediate

            playoff = list(filter(lambda game: self.getProperty(game, 'leaguePhase') == 'playoff', games))
            playoff = self.retrieveGroupedGamedays(playoff)
            result['playoff'] = playoff

        return result

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    # @param int limit
    #
    # @return dict
    def retrieveScheduleNextByLeague(self, league = None, limit = 15):
        if not league:
            return None

        today = self.getTodaysTimestamp()

        # Fetch all games in a flat list.
        games = self.retrieveScheduleListFlatByLeague(league)
        games = list(filter(lambda game: int(game['scheduledTimestamp']) > today, games))

        if games:
            # Group all games by their schedule date.
            games = self.retrieveGroupedGamedays(games)
            games = sorted(games, key=lambda k: k['date']['sortValue'])
            games = games[0:limit]

        result = {
            'games': games,
            'league': league['title']
        }

        return result

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    # @param int limit
    #
    # @return dict
    def retrieveScheduleLatestByLeague(self, league = None, limit = 15):
        if not league:
            return None

        today = self.getTodaysTimestamp()

        # Fetch all games in a flat list.
        games = self.retrieveScheduleListFlatByLeague(league)
        games = list(filter(lambda game: game['scheduledTimestamp'] < today, games))

        if games:
            # Group all games by their schedule date.
            games = self.retrieveGroupedGamedays(games)
            games = sorted(games, key=lambda k: k['date']['sortValue'], reverse=True)
            games = games[0:limit]

        result = {
            'games': games,
            'league': league['title']
        }

        return result

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    # @param int limit
    #
    # @return dict
    def retrieveScheduleTodayByLeague(self, league = None, limit = 15):
        if not league:
            return None

        today = self.getTodaysTimestamp()

        # Fetch all games in a flat list.
        games = self.retrieveScheduleListFlatByLeague(league, True)
        games = list(filter(lambda game: game['scheduledTimestamp'] == today, games))

        if games:
            # Sort the games.
            games = sorted(games, key=lambda k: k['scheduledDate']['sortValue'])
            games = games[0:limit]

        result = {
            'games': games,
            'league': league['title']
        }

        return result

    # Returns a list of schedule items. Without grouping.
    #
    # @param dict league
    #
    # @return list
    def retrieveScheduleListFlatByLeague(self, league = None, forceFetch = False):
        if not league:
            return None

        # Check if the schedule list is already in the cache.
        key = self.REDIS_KEY + str(league['id'])
        if not forceFetch:
            redisData = self.getCachedRedisDataByKey(key)
            if redisData:
                return redisData

        result = []

        # Fetch schedule for playoff round.
        if self.getProperty(league, 'playoff') and league['playoff']['divisionId']:
            divisionId = league['playoff']['divisionId']
            games = self.fetchHockeyDataSchedule(divisionId, 'playoff')
            if games:
                result = result + games

        # Fetch schedule for intermediate round.
        if self.getProperty(league, 'intermediate'):
            for item in league['intermediate']:
                games = self.fetchHockeyDataSchedule(item['divisionId'], 'intermediate')
                if games:
                    result = result + games

        # Fetch schedule for preliminary round.
        if self.getProperty(league, 'preliminary'):
            divisionId = league['preliminary']['divisionId']
            games = self.fetchHockeyDataSchedule(divisionId, 'preliminary')
            if games:
                result = result + games

        serializedData = []
        if result:
            for game in result:
                serializedData.append(game.toDict())

        # Cache the result.
        self.setCachedRedisDataByKey(key, serializedData, self.REDIS_CACHE_TIME)

        return serializedData

    # Returnes a grouped list of gamedays.
    #
    # @param list gameDays
    #
    # @return list
    def retrieveGroupedGamedays(self, gameDays = []):
        if not gameDays:
            return None

        result = []

        for game in gameDays:
            date = game['scheduledDate']
            timestamp = game['scheduledTimestamp']
            gameday = self.findGameDayByTimestamp(result, timestamp)

            if not gameday:
                date['scheduledTimestamp'] = timestamp
                gameday = {
                    'date': date,
                    'games': [game]
                }
                result.append(gameday)
            else:
                gameday['games'].append(game)

        if result:
            result = sorted(result, key=lambda k: k['date']['sortValue'])

        return result

    # Returnes a gameday out of a list of gamedays.
    #
    # @param list gameDays
    # @param int timestamp
    #
    # @return dict|None
    def findGameDayByTimestamp(self, gameDays = [], timestamp = None):
        for gameDay in gameDays:
            if gameDay['date']['scheduledTimestamp'] == timestamp:
                return gameDay

        return None

    # Returnes a list of compact game objects.
    #
    # @param list data
    #
    # @return list
    def retrieveReducedData(self, data = [], leaguePhase = 'preliminary'):
        if not data:
            return None

        result = []

        for item in data:
            item['leaguePhase'] = leaguePhase
            game = ScheduleItemModel(item)
            result.append(game)

        return result

    # Fetches all schedule items by a given divisionId.
    #
    # @param string divisionId
    #
    # @return list
    def fetchHockeyDataSchedule(self, divisionId = None, leaguePhase = 'preliminary'):
        if not divisionId:
            return None

        data = self.hockeyDataService.fetchHockeyDataSchedule(divisionId)

        if not data:
            return []

        # remove unused strucutre
        return self.retrieveReducedData(data, leaguePhase)

from hockeyservice.services.baseview import BaseView

class Game(BaseView):

    gameData = None
    homeTeamStats = None
    homePenalties = None
    homeGoals = None
    homeFieldPlayers = None
    homeGoalKeepers = None
    awayTeamStats = None
    awayPenalties = None
    awayGoals = None
    awayFieldPlayers = None
    awayGoalKeepers = None

    # Fetches data from api and prepares them to be responded.
    #
    # @param string gameId
    #
    # @return dict
    def retrieveGameById(self, gameId = None):
        if not gameId:
            return None

        game = self.hockeyDataService.fetchHockeyDataScheduleItem(gameId)
        if game:
            game = self.retrieveReducedData(game)

        return game

    # Extracts the main structure and builds the response dict.
    #
    # @param dict data
    #
    # @return dict
    def retrieveReducedData(self, data = {}):
        self.gameData = self.getProperty(data, 'gameData', {})
        self.homeTeamStats = self.getProperty(data, 'homeTeamStats', {})
        self.homePenalties = self.getProperty(data, 'homePenalties', [])
        self.homeGoals = self.getProperty(data, 'homeGoals', [])
        self.homeFieldPlayers = self.getProperty(data, 'homeFieldPlayers', [])
        self.homeGoalKeepers = self.getProperty(data, 'homeGoalKeepers', [])
        self.awayTeamStats = self.getProperty(data, 'awayTeamStats', {})
        self.awayPenalties = self.getProperty(data, 'awayPenalties', [])
        self.awayGoals = self.getProperty(data, 'awayGoals', [])
        self.awayFieldPlayers = self.getProperty(data, 'awayFieldPlayers', [])
        self.awayGoalKeepers = self.getProperty(data, 'awayGoalKeepers', [])

        result = {
            'overview': self.retrieveOverview(),
            'actions': self.retrieveActions(),
            'statistics': self.retrieveStatistics(),
            'officials': self.retrieveOfficials(),
            'lineup': self.retrieveLineup(),
        }

        return result

    # Returns the overview structure.
    #
    # @return dict
    def retrieveOverview(self, data = {}):
        result = {}
        result['id'] = self.getProperty(self.gameData, 'id')
        result['homeTeamId'] = self.getProperty(self.gameData, 'homeTeamId')
        result['homeTeamLongname'] = self.getProperty(self.gameData, 'homeTeamLongname')
        result['homeTeamShortname'] = self.getProperty(self.gameData, 'homeTeamShortname')
        result['homeTeamScore'] = self.getProperty(self.gameData, 'homeTeamScore')
        result['awayTeamId'] = self.getProperty(self.gameData, 'awayTeamId')
        result['awayTeamLongname'] = self.getProperty(self.gameData, 'awayTeamLongname')
        result['awayTeamShortname'] = self.getProperty(self.gameData, 'awayTeamShortname')
        result['awayTeamScore'] = self.getProperty(self.gameData, 'awayTeamScore')
        result['gameStatus'] = self.getProperty(self.gameData, 'gameStatus')
        result['periodStats'] = self.getProperty(self.gameData, 'periodStats')
        result['isOvertime'] = self.getProperty(self.gameData, 'isOvertime')
        result['isShootOut'] = self.getProperty(self.gameData, 'isShootOut')
        result['scheduledTime'] = self.getProperty(self.gameData, 'scheduledTime')
        result['scheduledDate'] = self.getProperty(self.gameData, 'scheduledDate')

        return result

    # Returns the action strucutre.
    #
    # @return list
    def retrieveActions(self):
        result = []

        for item in self.homeGoals:
            goal = self.retrieveReducedGoalData(item, True)
            result.append(goal)

        for item in self.awayGoals:
            goal = self.retrieveReducedGoalData(item, False)
            result.append(goal)

        for item in self.homePenalties:
            penalty = self.retrieveReducedPenaltyData(item, True)
            result.append(penalty)

        for item in self.awayPenalties:
            penalty = self.retrieveReducedPenaltyData(item, False)
            result.append(penalty)

        if result:
            result = sorted(result, key=lambda k: k['gameTimeFormatted'])

        return result

    # Returns the statistics structure.
    #
    # @param dict data
    #
    # @return dict
    def retrieveStatistics(self):
        result = {
            'location': None,
            'attendance': None,
            'homeShotsOnGoal': 0,
            'awayShotsOnGoal': 0,
            'homePenalties': 0,
            'awayPenalties': 0,
            'homePowerplayGoals': 0,
            'awayPowerplayGoals': 0,
            'homeTimeLeading': 0,
            'awayTimeLeading': 0,
            'homeTimeTied': 0,
            'awayTimeTied': 0,
            'homeTimeTrailing': 0,
            'awayTimeTrailing': 0
        }

        result['location'] = self.getProperty(self.gameData, 'location')
        result['attendance'] = self.getProperty(self.gameData, 'attendance')

        result['homeShotsOnGoal'] = self.getProperty(self.homeTeamStats, 'shotsOnGoal')
        timeValues = self.getProperty(self.homeTeamStats, 'timeValues', [])
        result['homeTimeLeading'] = self.getProperty(timeValues, 'leading')
        result['homeTimeTied'] = self.getProperty(timeValues, 'tied')
        result['homeTimeTrailing'] = self.getProperty(timeValues, 'trailing')

        result['awayShotsOnGoal'] = self.getProperty(self.awayTeamStats, 'shotsOnGoal')
        timeValues = self.getProperty(self.awayTeamStats, 'timeValues', [])
        result['awayTimeLeading'] = self.getProperty(timeValues, 'leading')
        result['awayTimeTied'] = self.getProperty(timeValues, 'tied')
        result['awayTimeTrailing'] = self.getProperty(timeValues, 'trailing')

        result['homePenalties'] = len(self.homePenalties)
        result['awayPenalties'] = len(self.awayPenalties)
        result['homePowerplayGoals'] = self.countPowerplayGoals(self.homeGoals)
        result['awayPowerplayGoals'] = self.countPowerplayGoals(self.awayGoals)

        return result

    # Returns the lineup structure.
    #
    # @return dict
    def retrieveLineup(self):
        result = {
            'home': {},
            'away': {},
        }

        # Build home players.
        result['home']['forwards'] = self.retrievePlayersListForwards(self.homeFieldPlayers)
        result['home']['defensemen'] = self.retrievePlayersListDefensemen(self.homeFieldPlayers)
        result['home']['goalkeepers'] = self.retrievePlayersListGoalkeepers(self.homeGoalKeepers)

        # Build away players.
        result['away']['forwards'] = self.retrievePlayersListForwards(self.awayFieldPlayers)
        result['away']['defensemen'] = self.retrievePlayersListDefensemen(self.awayFieldPlayers)
        result['away']['goalkeepers'] = self.retrievePlayersListGoalkeepers(self.awayGoalKeepers)

        # Build coaches list.
        teamOfficials = self.getProperty(self.gameData, 'teamOfficials', [])
        result['home']['coaches'] = self.retrieveCoachesList(
            [self.getProperty(teamOfficials, 'headcoachhome')]
        )
        result['away']['coaches'] = self.retrieveCoachesList(
            [self.getProperty(teamOfficials, 'headcoachaway')]
        )

        return result

    # Returns the officials structure.
    #
    # @return dict
    def retrieveOfficials(self):
        result = {
            'linesmen': [],
            'headref': [],
        }

        if not self.gameData:
            return result

        gameOfficials = self.getProperty(self.gameData, 'gameOfficials', [])
        if not gameOfficials:
            return result

        official = self.getProperty(gameOfficials, 'ref1')
        if official and self.getProperty(official, 'lastname'):
            result['headref'].append(self.retrieveReducedOfficialData(official))

        official = self.getProperty(gameOfficials, 'ref2')
        if official and self.getProperty(official, 'lastname'):
            result['headref'].append(self.retrieveReducedOfficialData(official))

        official = self.getProperty(gameOfficials, 'linesman1')
        if official and self.getProperty(official, 'lastname'):
            result['linesmen'].append(self.retrieveReducedOfficialData(official))

        official = self.getProperty(gameOfficials, 'linesman2')
        if official and self.getProperty(official, 'lastname'):
            result['linesmen'].append(self.retrieveReducedOfficialData(official))

        return result

    # Returns the amount of powerplay goals of a given goals list.
    #
    # @param list goals
    #
    # @return number
    def countPowerplayGoals(self, goals = []):
        validGoals = ['PP', 'PP1', 'PP2', 'PP3']
        result = 0

        for goal in goals:
            if self.getProperty(goal, 'gameStrength') in validGoals:
                result += 1

        return result

    # Returns a list of players.
    #
    # @param list players
    #
    # @return list
    def retrievePlayersListForwards(self, players = []):
        result = []
        mapping = ['RF', 'LF', 'C', 'F']

        for player in players:
            player = self.retrieveReducedPlayerData(player)

            if self.getProperty(player, 'position') in mapping:
                result.append(player)

        if result:
            return sorted(result, key=lambda k: k['number'])

        return result

    # Returns a list of players.
    #
    # @param list players
    #
    # @return list
    def retrievePlayersListDefensemen(self, players = []):
        result = []
        mapping = ['LD', 'RD', 'D']

        for player in players:
            player = self.retrieveReducedPlayerData(player)

            if self.getProperty(player, 'position') in mapping:
                result.append(player)

        if result:
            return sorted(result, key=lambda k: k['number'])

        return result

    # Returns a list of players.
    #
    # @param list players
    #
    # @return list
    def retrievePlayersListGoalkeepers(self, players = []):
        result = []
        for player in players:
            player = self.retrieveReducedPlayerData(player)
            result.append(player)

        if result:
            return sorted(result, key=lambda k: k['number'])

        return result

    # Returns a list of coaches.
    #
    # @param list coaches
    #
    # @return list
    def retrieveCoachesList(self, coaches = None):
        if not coaches:
            return None

        result = []
        for coach in coaches:
            coach = self.retrieveReducedOfficialData(coach)
            result.append(coach)

        return result

    # Returns a reduced api structure.
    #
    # @param dict data
    #
    # @return dict
    def retrieveReducedOfficialData(self, data = None):
        if not data:
            return None

        official = {
            'firstname': self.retrieveFormattedName(self.getProperty(data, 'firstname')),
            'lastname': self.retrieveFormattedName(self.getProperty(data, 'lastname'))
        }

        return official

    # Returns a reduced api structure.
    #
    # @param dict data
    #
    # @return dict
    def retrieveReducedPlayerData(self, data = {}):
        player = {
            'id': self.getProperty(data, 'id'),
            'firstname': self.retrieveFormattedName(self.getProperty(data, 'playerFirstname')),
            'lastname': self.retrieveFormattedName(self.getProperty(data, 'playerLastname')),
            'number': self.getProperty(data, 'playerJerseyNr'),
            'position': self.getProperty(data, 'position')
        }

        return player

    # Returns a reduced api structure.
    #
    # @param dict data
    # @param boolean isHome
    #
    # @return dict
    def retrieveReducedPenaltyData(self, data = {}, isHome = False):
        penalty = {
            'gameTimeFormatted': self.getProperty(data, 'gameTimeFormatted'),
            'startTimeFormatted': self.getProperty(data, 'startTimeFormatted'),
            'endTimeFormatted': self.getProperty(data, 'endTimeFormatted'),
            'penaltyLength': self.getProperty(data, 'penaltyLength'),
            'reason': self.getProperty(data, 'offence'),
            'isHome': isHome,
            'isGoal': False
        }

        if self.getProperty(data, 'offender'):
            penalty['player'] = self.retrieveReducedPlayerData(self.getProperty(data, 'offender'))

        return penalty

    # Returns a reduced api structure.
    #
    # @param dict data
    # @param boolean isHome
    #
    # @return dict
    def retrieveReducedGoalData(self, data = {}, isHome = False):
        goal = {
            'gameTimeFormatted': self.getProperty(data, 'gameTimeFormatted'),
            'isEmptyNet': self.getProperty(data, 'isEmptyNet'),
            'isPenaltyShot': self.getProperty(data, 'isPenaltyShot'),
            'gameTimePeriod': self.getProperty(data, 'gameTimePeriod'),
            'gameStrength': self.getProperty(data, 'gameStrength'),
            'newScore': self.getProperty(data, 'newScore'),
            'isHome': isHome,
            'isGoal': True,
        }

        if self.getProperty(data, 'scoredBy'):
            goal['scoredBy'] = self.retrieveReducedPlayerData(self.getProperty(data, 'scoredBy'))

        if self.getProperty(data, 'assistBy'):
            goal['assistBy'] = self.retrieveReducedPlayerData(self.getProperty(data, 'assistBy'))

        if self.getProperty(data, 'assist2By'):
            goal['assist2By'] = self.retrieveReducedPlayerData(self.getProperty(data, 'assist2By'))

        return goal

    # Removes all unused characters from a given name string.
    #
    # @param string name
    #
    # @return string | None
    def retrieveFormattedName(self, name = ''):
        if not name:
            return None

        name = name.lower()
        name = name.replace('*', '')
        name = name.replace('**', '')
        name = name.replace('(f)', '')
        name = name.replace('(j)', '')
        name = name.replace('(koop)', '')
        return name.strip().title()

from hockeyservice.services.baseview import BaseView
from hockeyservice.model.standingsitem import StandingsItemModel

class Standings(BaseView):

    # Fetches data from api and prepares them to be responded.
    #
    # @param dict league
    #
    # @return dict
    def retrieveStandingsByLeague(self, league = None):
        if not league:
            return None

        result = {
            'league': league['title'],
            'preliminary': None,
            'intermediate': None
        }

        #Fetch standings for preliminary round
        if self.getProperty(league, 'preliminary'):
            divisionId = league['preliminary']['divisionId']
            data = self.hockeyDataService.fetchHockeyDataStandings(divisionId)

            if data:
                result['preliminary'] = self.retrieveReducedData(data)

        #Fetch standings for intermediate round
        if self.getProperty(league, 'intermediate'):
            result['intermediate'] = []
            for item in self.getProperty(league, 'intermediate'):
                divisionId = item['divisionId']

                if data:
                    data = self.hockeyDataService.fetchHockeyDataStandings(divisionId)
                    leagueRound = {
                        'title': item['title'],
                        'divisionId': item['divisionId'],
                        'standings': self.retrieveReducedData(data)
                        }
                    result['intermediate'].append(leagueRound)

        return result

    # Returns a reduced api structure.
    #
    # @param list data
    #
    # @return list
    def retrieveReducedData(self, data = []):
        result = []

        for item in data:
            standingsItem = StandingsItemModel(item)
            result.append(standingsItem.toDict())

        return result

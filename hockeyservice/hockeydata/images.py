import os
import shutil
import requests

class HockeyDataImages():

    IMAGE_BASE_URL = 'https://api2.hockeydata.net/img/icehockey/ebel/team-logos/'
    HERE = os.path.abspath(os.path.dirname(__file__))
    FALLBACK_IMG = 'fallback-image.png'
    PATH_IMAGES = '../../images/'
    PATH_ASSETS = '../../assets/'

    def retrieveTeamImageByName(self, name = ''):
        image = self.retrieveStoredTeamImagePathByName(name)

        if image:
            return image

        return self.fetchAndCreateImageByName(name)

    def retrieveStoredTeamImagePathByName(self, name = ''):
        imagePath = self.retrieveTeamImagePathByName(name)
        if os.path.isfile(imagePath):
            return imagePath
        return None

    def fetchAndCreateImageByName(self, name = ''):
        url = self.IMAGE_BASE_URL + str(name)
        response = requests.get(url, stream=True)

        if response.status_code != 200:
            return self.retrieveFallbackImagePath()

        imagePath = self.retrieveTeamImagePathByName(name)

        if not os.path.exists(os.path.dirname(imagePath)):
            try:
                os.makedirs(os.path.dirname(imagePath))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        with open(imagePath, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response

        return imagePath

    def retrieveFallbackImagePath(self):
        fallbackImage = self.PATH_ASSETS + self.FALLBACK_IMG
        path = os.path.join(self.HERE, fallbackImage)

        return path

    def retrieveTeamImagePathByName(self, name = ''):
        imagePath = self.PATH_IMAGES + 'teams/' + name
        path = os.path.join(self.HERE, imagePath)

        return path

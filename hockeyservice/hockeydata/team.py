import time
import datetime
from hockeyservice.services.baseview import BaseView
from hockeyservice.model.scheduleitem import ScheduleItemModel
from hockeyservice.hockeydata.standings import Standings

class Team(BaseView):

    teamData = None
    teamStats = None
    teamRoster = None
    playerStats = None
    goalkeeperStats = None
    games = None

    # Fetches data from api and prepares them to be responded.
    #
    # @param string leagueId
    # @param string teamId
    #
    # @return dict
    def retrieveTeamById(self, leagueId = None, teamId = None):
        league = self.hockeyDataService.retrieveLeagueById(leagueId)

        if not league or not teamId or not leagueId:
            return None

        result = {}
        standingsService = Standings()
        # TODO: Fix this
        divisionId = league['preliminary']['divisionId']
        if league['playoff'] and league['playoff']['divisionId']:
            divisionId = league['playoff']['divisionId']
        data = self.hockeyDataService.fetchHockeyDataTeam(divisionId, teamId)

        if not data:
            return None

        result['team'] = self.retrieveReducedData(data)
        result['team']['standings'] = standingsService.retrieveStandingsByLeague(league)

        return result

    # Returns a reduced api structure.
    #
    # @param list data
    #
    # @return list
    def retrieveReducedData(self, data = []):
        self.teamData = self.getProperty(data, 'teamData', {})
        self.teamStats = self.getProperty(data, 'teamStats', {})
        self.teamRoster = self.getProperty(data, 'teamRoster', [])
        self.playerStats = self.getProperty(data, 'playerStats', [])
        self.goalkeeperStats = self.getProperty(data, 'goalkeeperStats', [])
        self.games = self.getProperty(data, 'games', [])

        result = {
            'roster': self.retrieveTeamRoster(),
            'information': self.retrieveTeamInformation(),
            'topscorer': self.retrieveTeamTopScorer(),
            'games': self.retrieveTeamGames(),
            'statistics': self.retrieveTeamStatistics(),
        }

        return result

    # Returns a list of players.
    #
    # @param list players
    #
    # @return list
    def retrieveTeamRoster(self):
        forwardsMapping = ['RF', 'LF', 'C', 'F']
        defensemenMapping = ['LD', 'RD', 'D']
        goalkeepersMapping = ['G']

        result = {
            'forwards': self.retrievePlayersList(self.playerStats, forwardsMapping),
            'defensemen': self.retrievePlayersList(self.playerStats, defensemenMapping),
            'goalkeepers': self.retrievePlayersList(self.goalkeeperStats, goalkeepersMapping),
        }

        return result

    # Returns a list of players.
    #
    # @param list players
    #
    # @return list
    def retrievePlayersList(self, players = [], mapping = []):
        if not players:
            players = self.teamRoster

        result = []

        for player in players:
            player = self.retrieveReducedPlayerData(player)

            if self.getProperty(player, 'position') in mapping:
                result.append(player)

        if result:
            return sorted(result, key=lambda k: k['number'])

        return result

    def retrieveTeamInformation(self):
        result = {
            'id': self.getProperty(self.teamData, 'id'),
            'teamLongname': self.getProperty(self.teamData, 'teamLongname'),
            'teamShortname': self.getProperty(self.teamData, 'teamShortname'),
            'teamFlavourname': self.getProperty(self.teamData, 'teamFlavourname'),
        }

        return result

    def retrieveTeamTopScorer(self):
        result = []

        players = sorted(self.playerStats, key=lambda k: k['points'], reverse=True)

        if players:
            players = players[0:3]
            for player in players:
                player = self.retrieveReducedPlayerData(player)
                result.append(player)

        return result

    def retrieveTeamGames(self):
        result = {
            'last': [],
            'next': []
        }

        today = self.getTodaysTimestamp()

        for game in self.games:
            gameObj = {
                'id': self.getProperty(game, 'id'),
                'scheduledDate': self.getProperty(game, 'scheduledDate'),
                'scheduledTime': self.getProperty(game, 'scheduledTime'),
                'scheduledTimestamp': self.retrieveScheduledTimestamp(self.getProperty(game, 'scheduledDate')),
                'homeTeamId': self.getProperty(self.teamData, 'id'),
                'homeTeamShortname': self.getProperty(self.teamData, 'teamShortname'),
                'awayTeamId': self.getProperty(game, 'opponentTeamId'),
                'awayTeamShortname': self.getProperty(game, 'opponentTeamShortname'),
                'score': self.getProperty(game, 'score'),
            }

            homeGame = self.getProperty(game, 'isHomeGame')

            if not homeGame:
                gameObj['homeTeamId'] = self.getProperty(game, 'opponentTeamId')
                gameObj['homeTeamShortname'] = self.getProperty(game, 'opponentTeamShortname')
                gameObj['awayTeamId'] = self.getProperty(self.teamData, 'id')
                gameObj['awayTeamShortname'] = self.getProperty(self.teamData, 'teamShortname')
                # Score needs to be reversed because there is a bug in the external api.
                gameObj['score'] = self.getProperty(game, 'score', '')[::-1]

            if gameObj['scheduledTimestamp'] < today:
                result['last'].append(gameObj)
                continue

            result['next'].append(gameObj)

        result['last'] = sorted(result['last'], key=lambda k: k['scheduledDate']['sortValue'])
        result['next'] = sorted(result['next'], key=lambda k: k['scheduledDate']['sortValue'])
        return result

    def retrieveTeamStatistics(self):
        gamesTotal = len(self.games)
        gamesPlayed = self.getProperty(self.teamStats, 'gamesPlayed')

        result = {
            'gamesTotal': gamesTotal,
            'gamesPlayed': gamesPlayed,
            'gamesPlayedPercentage': self.calcPercentage(gamesTotal, gamesPlayed),
            'powerplays': 0,
            'powerplayGoals': 0,
            'powerplayEfficiency': self.getProperty(self.teamStats, 'powerplayPercentage'),
            'penalties': 0,
            'penaltiesDefended': 0,
            'penaltyGoals': self.getProperty(self.teamStats, 'powerplayGoalsAgainst'),
            'penaltykilling': self.getProperty(self.teamStats, 'penaltyKillingPercentage'),
            'shorthandedGoalsAgainst': self.getProperty(self.teamStats, 'shorthandedGoalsAgainst'),
            'shorthandedGoalsFor': self.getProperty(self.teamStats, 'shorthandedGoalsFor'),
            'penaltyMinutes': self.getProperty(self.teamStats, 'penaltyMinutes'),
            'minorPenalties': self.getProperty(self.teamStats, 'minorPenalties'),
            'majorPenalties': self.getProperty(self.teamStats, 'majorPenalties'),
        }

        powerplay = self.getProperty(self.teamStats, 'powerplay')
        if powerplay:
            powerplay = powerplay.split('/')
            result['powerplayGoals'] = int(powerplay[0])
            result['powerplays'] = int(powerplay[1])

        penalties = self.getProperty(self.teamStats, 'penaltykill')
        if penalties:
            penalties = penalties.split('/')
            result['penaltiesDefended'] = int(penalties[0])
            result['penalties'] = int(penalties[1])

        return result

    # Returns a reduced api structure.
    #
    # @param dict data
    #
    # @return dict
    def retrieveReducedPlayerData(self, data = {}):
        player = {
            'id': self.getProperty(data, 'id'),
            'firstname': self.retrieveFormattedName(self.getProperty(data, 'playerFirstname')),
            'lastname': self.retrieveFormattedName(self.getProperty(data, 'playerLastname')),
            'number': self.getProperty(data, 'playerJerseyNr'),
            'position': self.getProperty(data, 'position'),
            'gamesPlayed': self.getProperty(data, 'gamesPlayed', 0),
            'goals': self.getProperty(data, 'goals', 0),
            'assists': self.getProperty(data, 'assists', 0),
            'points': self.getProperty(data, 'points', 0),
            'shotsAgainst': self.getProperty(data, 'shotsAgainst', 0),
            'savePercentage': self.getProperty(data, 'savePercentage', 0),
            'playingTime': self.getProperty(data, 'playingTime', 0),
            'penaltyMinutes': self.getProperty(data, 'penaltyMinutes', 0),
            'penaltyMinutesPerGame': self.getProperty(data, 'penaltyMinutesPerGame', 0),
            'birthdate': self.getProperty(data, 'birthdate'),
            'goalsAgainst': self.getProperty(data, 'goalsAgainst', 0),
            'goalsAgainstAverage': self.getProperty(data, 'goalsAgainstAverage', 0),
        }

        return player

    # Removes all unused characters from a given name string.
    #
    # @param string name
    #
    # @return string | None
    def retrieveFormattedName(self, name = ''):
        if not name:
            return None

        name = name.lower()
        name = name.replace('*', '')
        name = name.replace('**', '')
        name = name.replace('(f)', '')
        name = name.replace('(j)', '')
        name = name.replace('(koop)', '')

        return name.strip().title()


    # @param int total
    # @param int current
    #
    # @return float
    def calcPercentage(self, total, current):
        result = (current * 100) / total

        return round(result, 2)

    # Returns a timestamp of the scheduled date.
    #
    # @return int
    def retrieveScheduledTimestamp(self, scheduledDate):
        if not scheduledDate or not scheduledDate['value']:
            return int(round(time.time() * 1000))

        date = datetime.datetime.strptime(scheduledDate['value'], "%d.%m.%Y")
        timestamp = time.mktime(date.timetuple())

        return int(round(timestamp * 1000))

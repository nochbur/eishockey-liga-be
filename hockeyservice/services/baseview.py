# ========
# BaseView
# ========
#
# This class is used for all main controllers as base class.

import time
import datetime
from hockeyservice.services.hockeydataservice import HockeyDataService
from hockeyservice.services.restservice import RestService
from hockeyservice.services.storage import Storage

class BaseView(RestService, Storage):

    # Init the class.
    def __init__(self):
        super(BaseView, self).__init__()
        self.hockeyDataService = self.getHockeyDataService

    @property
    def getHockeyDataService(self):
        return HockeyDataService()

    # Returns the property value for some dict or list.
    #
    # @param dict|list item
    # @param string|int property
    #
    # @return mixed|None
    def getProperty(self, item = None, prop = '', fallback = None):
        if not item:
            return fallback
        try:
            return item[prop]
        except KeyError:
            return fallback
        except TypeError:
            return fallback

    # Returns a json error object.
    #
    # @param string id
    #
    # @return dict
    def renderLeagueNotFoundError(self, leagueId = ''):
        errorMsg = 'No league found for id: {0}'.format(leagueId)

        return self.renderJsonErrorResponse(errorMsg)

    # Returns a timestamp of today
    #
    # @return int
    def getTodaysTimestamp(self):
        today = datetime.datetime.now().strftime("%d.%m.%Y")
        date = datetime.datetime.strptime(today, "%d.%m.%Y")
        timestamp = time.mktime(date.timetuple())
        return int(round(timestamp * 1000))

# ============
# Rest Service
# ============
#
# This simple class provides some helper functions to return a standard json object.

class RestService(object):

    DEFAULT_API_ERROR = 'Something went wrong with the api.'

    # Init the class.
    def __init__(self):
        super(RestService, self).__init__()

    # Creates a dict which is then rendered as json response.
    #
    # @param dict data
    #
    # @return dict
    def renderJsonResponse(self, data = None):
        response = {
            'status': 200,
            'data': data
        }

        return response

    # Creates a dict which is then rendered as error json response.
    #
    # @param string errorMsg
    #
    # @return dict
    def renderJsonErrorResponse(self, errorMsg = None):
        if not errorMsg:
            errorMsg = self.DEFAULT_API_ERROR

        response = {
            'status': 400,
            'data': None,
            'message': errorMsg
        }

        return response

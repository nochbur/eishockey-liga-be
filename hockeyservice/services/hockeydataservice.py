# ===================
# Hockey Data Service
# ===================
#
# This class handles the api calls to the hockeydata enpoints.

import os
import json
import requests
import time
import urllib
import logging

class HockeyDataService(object):

    API_BASE_URL = 'http://api.hockeydata.net/data/ebel'
    API_KEY = '2ab836fc49f83f130599c1d6dd556594'
    API_LOCALE = 'de'
    API_REFERER = 'app.h-sc.at'
    TIMESTAMP = int(time.time())
    DEFAULT_API_TIMEOUT = 30
    log = logging.getLogger(__name__)

    # Init the class.
    def __init__(self):
        super(HockeyDataService, self).__init__()
        self.defaultUrlParams = {
            'apiKey': self.API_KEY,
            'referer': self.API_REFERER,
            'locale': self.API_LOCALE,
            'timestamp': self.TIMESTAMP
        }

    # Fetch from knockout api endpoint.
    #
    # @param string divisionId
    #
    # @return Json | None
    def fetchHockeyDataKnockout(self, divisionId = None):
        url = self.retrieveKnockoutUrl(divisionId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']['phases']

        return data

    # Fetch from standings api endpoint.
    #
    # @param string divisionId
    #
    # @return Json | None
    def fetchHockeyDataStandings(self, divisionId = None):
        url = self.retrieveStandingsUrl(divisionId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']['rows']

        return data

    # Fetch from schedule api endpoint.
    #
    # @param string divisionId
    #
    # @return Json | None
    def fetchHockeyDataSchedule(self, divisionId = None):
        url = self.retrieveScheduleUrl(divisionId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']['rows']
        return data

    # Fetch from game api endpoint.
    #
    # @param string gameId
    #
    # @return Json | None
    def fetchHockeyDataScheduleItem(self, gameId = None):
        url = self.retrieveGameUrl(gameId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']
        return data

    # Fetch from team api endpoint.
    #
    # @param string divisionId
    # @param string gameId
    #
    # @return Json | None
    def fetchHockeyDataTeam(self, divisionId = None, teamId = None):
        url = self.retrieveTeamUrl(divisionId, teamId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']
        return data

    # Fetch from player api endpoint.
    #
    # @param string divisionId
    # @param string playerId
    #
    # @return Json | None
    def fetchHockeyDataPlayer(self, divisionId = None, playerId = None):
        url = self.retrievePlayerUrl(divisionId, playerId)
        data = self.fetchHockeyDataByUrl(url)

        # remove unused strucutre
        if data:
            return data['data']
        return data

    # Basic api function to fetch from a given url.
    #
    # @param string url
    #
    # @return Json | None
    def fetchHockeyDataByUrl(self, url = ''):
        if not url:
            self.log.error('No hockeydata url given.')

            return None

        try:
            self.log.info('Fetching data for: %s' % url)
            hockeydata = requests.get(url, timeout=self.DEFAULT_API_TIMEOUT)
            data = hockeydata.json()

            return data
        except requests.ConnectionError:
            self.log.error('HockeyData does not respond. %s' % url)

            return None
        except ValueError:
            self.log.error('HockeyData ValueError. %s' % url)

            return None

    # Returns the league config from the json file.
    #
    # @return Json
    def getLeagueConfig(self):
        here = os.path.abspath(os.path.dirname(__file__))
        with open(os.path.join(here, '../config/leagues-group.json')) as data_file:
            data = json.load(data_file)

        return data

    # Returns the league config for a given id.
    #
    # @param int id
    #
    # @return Json | None
    def retrieveLeagueById(self, id = None):
        config = self.getLeagueConfig();

        for group in config:
            for league in group['leagues']:
                if league['id'] == id:
                    return league

        return None

    # Returns the schedule url.
    #
    # @param string divisionId
    #
    # @return string
    def retrieveScheduleUrl(self, divisionId = None):
        return self.API_BASE_URL + '/Schedule?' + self.retrieveUrlParamsByDivisionId(divisionId)

    # Returns the standings url.
    #
    # @param string divisionId
    #
    # @return string
    def retrieveStandingsUrl(self, divisionId = None):
        return self.API_BASE_URL + '/Standings?' + self.retrieveUrlParamsByDivisionId(divisionId)

    # Returns the knockout url.
    #
    # @param string divisionId
    #
    # @return string
    def retrieveKnockoutUrl(self, divisionId = None):
        return self.API_BASE_URL + '/KnockoutStage?' + self.retrieveUrlParamsByDivisionId(divisionId)

    # Returns the team url.
    #
    # @param string divisionId
    # @param string teamId
    #
    # @return string
    def retrieveTeamUrl(self, divisionId = None, teamId = None):
        params = {
            'divisionId': divisionId,
            'teamId': teamId
        }

        return self.API_BASE_URL + '/GetTeamDetails?' + self.retrieveUrlParams(params)

    # Returns the game url.
    #
    # @param string gameId
    #
    # @return string
    def retrieveGameUrl(self, gameId = None):
        params = {
            'gameId': gameId
        }

        return self.API_BASE_URL + '/GetGameReport?' + self.retrieveUrlParams(params)

    # Returns the player url.
    #
    # @param string divisionId
    # @param string teamId
    #
    # @return string
    def retrievePlayerUrl(self, divisionId = None, playerId = None):
        params = {
            'divisionId': divisionId,
            'playerId': playerId
        }

        return self.API_BASE_URL + '/GetPlayerDetails?' + self.retrieveUrlParams(params)

    # Returns all default url params.
    #
    # @param string divisionId
    #
    # @return string
    def retrieveUrlParamsByDivisionId(self, divisionId = None):
        params = {
            'divisionId': divisionId
        }

        return self.retrieveUrlParams(params)

    # Returns all default url params merged with a given dict of further params.
    #
    # @param dict params
    #
    # @return string
    def retrieveUrlParams(self, params = {}):
        if params and isinstance(params, dict):
            params.update(self.defaultUrlParams)
            return urllib.parse.urlencode(params)

        return urllib.parse.urlencode(self.defaultUrlParams)

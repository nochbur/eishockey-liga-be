# ===============
# Storage Service
# ===============
#
# This services uses Redis 'https://redis.io/' to to create a key value storage.
# As all data is hold in the cache make sure you use this service wisely.

import json
import redis
import time

class Storage(object):

    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    REDIS_DB = 0
    REDIS_KEY = ''
    ENCODING = 'utf-8'
    DEFAULT_CACHE_TIME = 300000 #5min

    # Init the storage service.
    def __init__(self):
        super(Storage, self).__init__()
        self.storage = self.redisStorage

    # Provide the storage as property.
    @property
    def redisStorage(self):
        return redis.StrictRedis(host=self.REDIS_HOST, port=self.REDIS_PORT, db=self.REDIS_DB)

    # Returns a storage object as json.
    #
    # @param string key
    #
    # @return Json | None
    def getJsonStorageByKey(self, key = None):
        if not key or not self.storage:
            return None
        try:
            if self.storage.get(key):
                return json.loads(self.storage.get(key).decode(self.ENCODING))
        except redis.exceptions.ConnectionError:
            return None
        return None

    # Stores a given json object with the given key.
    #
    # @param string key
    # @param Json value
    #
    # @return Json | None
    def setJsonStorageByKey(self, key = None, value = None):
        if not key or not value or not self.storage:
            return None

        json_value = json.dumps(value)

        try:
            self.storage.set(key, json_value)
        except redis.exceptions.ConnectionError:
            # If the service is not available, return the given value.
            return value

        return self.getJsonStorageByKey(key)

    # Returns a storage object as json.
    #
    # @param string key
    #
    # @return Json | None
    def getCachedRedisDataByKey(self, key = None):
        redisData = self.getJsonStorageByKey(key)
        if not redisData:
            return None

        current_timestamp = self.getCurrentTimestamp()

        if redisData['expire_timestamp'] > current_timestamp:
            return redisData['data']

        return None

    # Returns a storage object as json.
    #
    # @param string key
    #
    # @return Json | None
    def setCachedRedisDataByKey(self, key = None, value = None, max_cache_time = None):
        if not key or not value:
            return None

        if not max_cache_time:
            max_cache_time = self.DEFAULT_CACHE_TIME

        expire_timestamp = self.getCurrentTimestamp() + max_cache_time

        cachedData = {
            'data': value,
            'expire_timestamp': expire_timestamp
        }

        return self.setJsonStorageByKey(key, cachedData)

    # Returns the current timestamp in miliseconds.
    #
    # @return Json | None
    def getCurrentTimestamp(self):
        return int(round(time.time() * 1000))

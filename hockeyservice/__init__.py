from pyramid.config import Configurator
from pyramid.renderers import JSONP

# Main function to start the app and register all routes.
def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_renderer('jsonp', JSONP(param_name='callback'))
    config.add_route('leagues', '/leagues')
    config.add_route('standings', '/leagues/{id}/standings')
    config.add_route('scheduleList', '/leagues/{id}/schedule')
    config.add_route('scheduleNextList', '/leagues/{id}/schedule-next')
    config.add_route('scheduleLatestList', '/leagues/{id}/schedule-latest')
    config.add_route('scheduleTodayList', '/leagues/{id}/schedule-today')
    config.add_route('scheduleCacheWarmup', '/leagues/schedule-cache-warmup')
    config.add_route('gameDetailView', '/leagues/{id}/game/{gameId}')
    config.add_route('knockout', '/leagues/{id}/knockout')
    config.add_route('team', '/leagues/{id}/teams/{teamId}')
    config.add_route('player', '/leagues/{id}/players/{playerId}')
    config.add_route('teamLogo', '/images/teams/{imageName}')
    config.add_route('fetchNews', '/news')
    config.scan()

    return config.make_wsgi_app()

import scrapy
from hockeyservice.spiders.NewsSpiderBase import NewsSpiderBase

class NewsSpiderBlick(NewsSpiderBase):
    name = "news"
    start_urls = [
        'https://www.blick.ch/sport/eishockey/',
    ]

    def retrieveCssSelectorForElement(self, element):
        selectors = {
            'newsTeaser': 'div.widget_newsListTeaser',
            'newsItem': 'div.item',
            'link': 'a::attr("href")',
            'teaserImage': 'a img::attr("src")',
            'headline': 'a::attr("title")',
            'excerpt': 'p.overview::text',
            'articleImage': 'div.slide-image img::attr("src")',
        }

        if element in selectors:
            return selectors[element]

        return ''

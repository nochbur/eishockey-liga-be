import scrapy

class NewsSpiderBase(scrapy.Spider):
    name = "news"
    start_urls = []

    def parse(self, response):
        newsTeaser = response.css(self.retrieveCssSelectorForElement('newsTeaser'))

        if not newsTeaser:
            return

        for item in newsTeaser.css(self.retrieveCssSelectorForElement('newsItem')):
            data = self.retrieveNewsItemData(item)
            request = scrapy.Request(data['link'], callback=self.parseDetailPage)
            request.meta['data'] = data

            yield request

    def retrieveNewsItemData(self, item):
        result = {
            'link': '',
            'teaserImage': '',
            'headline': '',
            'excerpt': '',
            'articleImage': ''
        }

        link = item.css(self.retrieveCssSelectorForElement('link')).extract_first()
        if link:
            result['link'] = link

        teaserImage = item.css(self.retrieveCssSelectorForElement('teaserImage')).extract_first()
        if teaserImage:
            result['teaserImage'] = teaserImage

        headline = item.css(self.retrieveCssSelectorForElement('headline')).extract_first()
        if headline:
            result['headline'] = headline.strip(' \t\n\r')

        excerpt = item.css(self.retrieveCssSelectorForElement('excerpt')).extract_first()
        if excerpt:
            excerpt.strip('\t\n\r')
            result['excerpt'] = excerpt.strip(' \t\n\r')

        return result

    def parseDetailPage(self, response):
        item = response.meta['data']
        articleImage = response.css(self.retrieveCssSelectorForElement('articleImage')).extract_first()

        if articleImage:
            item['articleImage'] = articleImage

        yield item

    def retrieveCssSelectorForElement(self, element):
        selectors = {
            'newsTeaser': '',
            'newsItem': '',
            'link': '',
            'teaserImage': '',
            'headline': '',
            'excerpt': '',
            'articleImage': '',
        }

        if element in selectors:
            return selectors[element]

        return ''

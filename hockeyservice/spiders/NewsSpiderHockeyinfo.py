import scrapy
from hockeyservice.spiders.NewsSpiderBase import NewsSpiderBase

class NewsSpiderHockeyinfo(NewsSpiderBase):
    name = "news"
    start_urls = [
        'http://www.hockey-news.info/category/oesterreich/ebel/',
    ]

    def retrieveCssSelectorForElement(self, element):
        selectors = {
            'newsTeaser': 'ul.infinite-content',
            'newsItem': 'li.infinite-post',
            'link': 'a::attr("href")',
            'teaserImage': 'div.archive-list-img img.reg-img::attr("src")',
            'headline': 'a::attr("title")',
            'excerpt': 'div.archive-list-text p::text',
            'articleImage': 'div#post-feat-img img.wp-post-image::attr("data-lazy-src")',
        }

        if element in selectors:
            return selectors[element]

        return ''

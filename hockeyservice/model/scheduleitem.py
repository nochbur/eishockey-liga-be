# ===================
# Schedule Item Model
# ===================

import time
import datetime
from hockeyservice.model.basemodel import BaseModel

class ScheduleItemModel(BaseModel):

    def createModel(self, item = {}):
        self.id = self.getProperty(item, 'id')
        self.homeTeamId = self.getProperty(item, 'homeTeamId')
        self.homeTeamLongName = self.getProperty(item, 'homeTeamLongName')
        self.homeTeamShortName = self.getProperty(item, 'homeTeamShortName')
        self.homeTeamScore = self.getProperty(item, 'homeTeamScore')
        self.awayTeamId = self.getProperty(item, 'awayTeamId')
        self.awayTeamLongName = self.getProperty(item, 'awayTeamLongName')
        self.awayTeamShortName = self.getProperty(item, 'awayTeamShortName')
        self.awayTeamScore = self.getProperty(item, 'awayTeamScore')
        self.isOvertime = self.getProperty(item, 'isOvertime')
        self.isShootOut = self.getProperty(item, 'isShootOut')
        self.scheduledTime = self.getProperty(item, 'scheduledTime')
        self.scheduledDate = self.getProperty(item, 'scheduledDate')
        self.gameStatus = self.getProperty(item, 'gameStatus')
        self.leaguePhase = self.getProperty(item, 'leaguePhase')
        self.periodStats = self.getProperty(item, 'periodResults')
        self.gameHasEnded = self.getProperty(item, 'gameHasEnded')
        self.dateIsToBeDetermined = self.getProperty(item, 'dateIsToBeDetermined')
        self.scheduledTimestamp = self.retrieveScheduledTimestamp()

    # Returns a timestamp of the scheduled date.
    #
    # @return int
    def retrieveScheduledTimestamp(self):
        if not self.scheduledDate or not self.scheduledDate['value']:
            return int(round(time.time() * 1000))

        date = datetime.datetime.strptime(self.scheduledDate['value'], "%d.%m.%Y")
        timestamp = time.mktime(date.timetuple())

        return int(round(timestamp * 1000))


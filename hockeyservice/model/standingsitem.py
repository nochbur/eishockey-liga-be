# ===================
# Standings Item Model
# ===================

from hockeyservice.model.basemodel import BaseModel

class StandingsItemModel(BaseModel):

    def createModel(self, item = {}):
        self.id = self.getProperty(item, 'id')
        self.teamLongname = self.getProperty(item, 'teamLongname')
        self.teamShortname = self.getProperty(item, 'teamShortname')
        self.tableRank = self.getProperty(item, 'tableRank')
        self.gamesWon = self.getProperty(item, 'gamesWon')
        self.gamesWonInOt = self.getProperty(item, 'gamesWonInOt')
        self.gamesLostInOt = self.getProperty(item, 'gamesLostInOt')
        self.gamesLost = self.getProperty(item, 'gamesLost')
        self.goalsFor = self.getProperty(item, 'goalsFor')
        self.goalsAgainst = self.getProperty(item, 'goalsAgainst')
        self.goalDifference = self.getProperty(item, 'goalDifference')
        self.gamesPlayed = self.getProperty(item, 'gamesPlayed')
        self.points = self.getProperty(item, 'points')

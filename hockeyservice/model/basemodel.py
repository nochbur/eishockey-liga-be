# ===================
# Schedule Item Model
# ===================

from hockeyservice.services.baseview import BaseView

class BaseModel(object):

    def __init__(self, item = {}):
        super(BaseModel, self).__init__()
        self.createModel(item)

    def createModel(self, item = {}):
        return

    # Returns the property value for some dict or list.
    #
    # @param dict|list item
    # @param string|int property
    #
    # @return mixed|None
    def getProperty(self, item = None, prop = '', fallback = None):
        return BaseView().getProperty(item, prop, fallback)

    def toDict(self):
        return self.__dict__

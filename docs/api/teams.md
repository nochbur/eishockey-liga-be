# Teams Api

## GET

**URL:** `/leauges/<leaguesId>/teams/<teamId>`

**Response**

```
{
    "information": {
        "id": <int>,
        "teamLongname": <string>,
        "teamShortname": <string>,
        "teamFlavourname": <string>
    },
    "roster": {
        "forwards": [
            {
                "id": <int>,
                "firstname": <string>,
                "lastname": <string>,
                "number": <int>,
                "position": <string>,
                "gamesPlayed": <int>,
                "goals": <int>,
                "assists": <int>,
                "points": <int>,
                "goalsAgainst": <int>,
                "goalsAgainstAverage": <int>
            },
        ],
        "defensemen": [
            {
                "id": <int>,
                "firstname": <string>,
                "lastname": <string>,
                "number": <int>,
                "position": <string>,
                "gamesPlayed": <int>,
                "goals": <int>,
                "assists": <int>,
                "points": <int>,
                "goalsAgainst": <int>,
                "goalsAgainstAverage": <int>
            },
        ],
        "goalkeepers": [
            {
                "id": <int>,
                "firstname": <string>,
                "lastname": <string>,
                "number": <int>,
                "position": <string>,
                "gamesPlayed": <int>,
                "goals": <int>,
                "assists": <int>,
                "points": <int>,
                "goalsAgainst": <int>,
                "goalsAgainstAverage": <int>
            },
        ],
    },
    "topscorer": [
        {
            "id": <int>,
            "firstname": <string>,
            "lastname": <string>,
            "number": <int>,
            "position": <string>,
            "gamesPlayed": <int>,
            "goals": <int>,
            "assists": <int>,
            "points": <int>,
            "goalsAgainst": <int>,
            "goalsAgainstAverage": <int>
        },
    ],
    "games": {
        "last": [
            {
                "id": <string>,
                "scheduledDate": {
                    "sortValue": <int>,
                    "value": <string>,
                    "shortValue": <string>,
                    "longValue": <string>
                },
                "scheduledTime": <string>,
                "homeTeamId": <int>,
                "homeTeamShortname": <string>,
                "awayTeamId": <int>,
                "awayTeamShortname": <string>,
                "score": <string>
            }
        ]
        "next": [
            {
                "id": <string>,
                "scheduledDate": {
                    "sortValue": <int>,
                    "value": <string>,
                    "shortValue": <string>,
                    "longValue": <string>
                },
                "scheduledTime": <string>,
                "homeTeamId": <int>,
                "homeTeamShortname": <string>,
                "awayTeamId": <int>,
                "awayTeamShortname": <string>,
                "score": <string>
            }
        ]
    }
    "statistics": {
        "games": <int>,
        "powerplayChances": <int>,
        "powerplayGoals": <int>,
        "powerplayEfficiency": <float>,
        "penalties": <int>,
        "penaltiesDefended": <int>,
        "penaltyGoals": <int>,
        "penaltykilling": <float>,
        "shorthandedGoalsAgainst": <int>,
        "shorthandedGoalsFor": <int>,
        "penaltyMinutes": <int>,
        "minorPenalties": <int>,
        "majorPenalties": <int>,
        "powerplays": <int>
    }
}
```

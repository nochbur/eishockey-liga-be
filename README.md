hockeyService
=============

Intstall
--------

Install Redis

```
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
make install
```

```
brew install python3
```

Create a Python virtual environment.

```
python3 -m venv env
```

Upgrade packaging tools.

```
env/bin/pip install --upgrade pip setuptools
```

Install the projekct dependencies.

```
env/bin/pip install -e .
```

Install the project in editable mode with its testing requirements.

```
env/bin/pip install -e ".[testing]"
```

Run your project's tests.

```
env/bin/pytest
```

Run your project.

```
env/bin/pserve development.ini
```

Run Redis
```
redis-server
```

Clear all stored redis objects
```
redis-cli flushall
```
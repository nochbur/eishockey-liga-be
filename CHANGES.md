# Changes Hockeyservice

##### unreleased

##### 2017-12-13 0.1.2

- BUGFIX: Fixed error when hockeydata service is not available.
- BUGFIX: Fixed caching of live games.

##### 2017-11-13 0.1.1

- BUGFIX: Fixed redis key of team and player page.

##### 2017-11-09 0.1.0

- FEATURE: Added redis support for team and player.
- BUGFIX: Fixed game officials.

##### 2017-10-26 0.0.1

- FEATURE: Added player api.
- FEATURE: Added team api.
- Initial version.
